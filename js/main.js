$(document).ready(function () {
  $('.modal').modal();
});

$(document).ready(function () {
  $('select').formSelect();
});

$(document).ready(function () {
  $('.tabs').tabs();
});


document.addEventListener('DOMContentLoaded', function () {
  var elems = document.querySelectorAll('.datepicker')
  var options = {
    firstDay: true,
    yearRange: [1940, 2030],
    format: 'dd-mm-yyyy',
    i18n: {
      months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
      monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
      weekdays: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
      weekdaysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
      weekdaysAbbrev: ["D", "L", "M", "M", "J", "V", "S"],
      cancel: "Cancelar",
    }
  };
  var instances = M.Datepicker.init(elems, options);
});

function mostrarcontenido() {
  var x = document.getElementsByClassName("con-oculto");
  for (let index = 0; index < x.length; index++) {
    if (x[index].style.display === "block") {
      x[index].style.display = "none";
    } else {
      x[index].style.display = "block";
    }
  }
}

if ($('.ty-compact-list').length > 3) {
  $('.show-more').show();
}

$('.show-more').on('click', function () {

  $(this).text() === 'Menos detalles' ?
    (
      $(this).text('Más detalles'),
      $(this).append("<i class=\"fas fa-angle-down\" style=\"margin: 5px; font-size: 18px;\"></i>")
    ) : (
      $(this).text('Menos detalles'),
      $(this).append("<i class=\"fas fa-angle-up\" style=\"margin: 5px; font-size: 18px;\"></i>")
    );
});


$(function () {
  $(".checkbox").click(function () {
    $('.delete').prop('disabled', $('input.checkbox:checked').length == 0);
    $('.delete').removeClass('desactivado');
  });
});

$(function () {
  $(".checkbox1").click(function () {
    $('.flex').prop('disabled', $('input.checkbox:checked').length == 0);
    $('.delete').toggleClass('nav-desactivo');
    $('.delete').toggleClass('nav-activo');
    $('.activacion').toggleClass('grey-text');
    $('.activacion').toggleClass('red-text');
  });
});

$(document).ready(function(){
  $('.sidenav').sidenav();
});

//animacion waves
for(var i = 0; i < 110; i++){
$('#waves').append('<div class="wave-bar"></div>');}

//TASK TIME
timeNow = function() {
  const today = new Date();
  let hour = today.getHours();
  let min = today.getMinutes();
  const clock = document.getElementById('clock');
  if (hour < 10) {
    hour = '0' + hour;
  }
  if (min < 10) {
    min = '0' + min;
  }

  clock.textContent = `${hour}:${min}`;
};

//Task Date
setInterval(timeNow, 1000);

var day =  {weekday: 'long'};
var wd =  new Date().toLocaleDateString('esp', day);

var dMonth =  {day: 'numeric'};
var md=  new Date().toLocaleDateString('esp', dMonth);

var month= { month: 'long'};
var m =  new Date().toLocaleDateString('esp',month);

document.getElementById("today").innerHTML = wd +" "+md+" de "+m;

document.getElementById("month").innerHTML =m;


$('#waves').append('<div class="wave-bar"></div>');
